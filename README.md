# LaxusAirportApp

Proyecto de prueba consumiento el api https://www.instantwebtools.net/fake-rest-api

## Introducción

- El app funciona como el panel admin de un aeropuerto, donde puedes agregar aerolineas mas no eliminarlas, tambien puedes leer, editar agregar y eliminar pasajeros de su tabla, lo que hace que el proyecto sea algo dinamicol.

## Inconvenientes

- El endpoint de aerolinas no tiene paginación lo que dificilta encontrar la que creemos 
- La creación de aerolineas es infinita, por lo que sumado al punto anterior, hacen de nuestro proyecto un problema.

## Solución

- Realize un bucle inverso, donde muestro las ultimas 20 aerolineas luego de ser cargadas, haciendo que las aerolineas que creemos se vean de primeras y solo limitando a 20 la cantidad de aerolineas, debido a que si se intenta cargar todas, el app se vuelve lenta y hace que la experiencia de uso sea desagradable, debido a que tiene mas de 12000 objetos, la gran mayoria con imagenes.


