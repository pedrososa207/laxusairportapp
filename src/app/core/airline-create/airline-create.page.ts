import { AlertsService } from './../../services/alerts.service';
import { EndpointsService } from './../../services/endpoints.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-airline-create',
  templateUrl: './airline-create.page.html',
  styleUrls: ['./airline-create.page.scss'],
})
export class AirlineCreatePage implements OnInit {
  airlineForm: FormGroup;
  submitted: boolean;
  loader: boolean = false;
  constructor(
    private api: EndpointsService,
    private builder: FormBuilder,
    private alert: AlertsService
  ) {}
  ngOnInit(): void {
    this.loadForm();
    this.getAirlineList();
  }
  // funcion para obtener el numero total de aerolineas registradas en la base de datos, para a partir de ese numero genear un ID para la siguiente aerolinea
  getAirlineList() {
    this.loader = true;
    this.api.getAllAirlines().subscribe(
      (_airlines: any) => {
        this.airlineForm.controls['id'].patchValue(_airlines.length + 1);
        this.loader = false;
      },
      (err) => {
        this.loader = false;
      }
    );
  }
  // creación del formulario donde se llena la data de la aerolinea
  loadForm() {
    this.airlineForm = this.builder.group({
      id: [''],
      name: ['', [Validators.required]],
      country: ['', [Validators.required]],
      logo: ['', [Validators.required]],
      slogan: ['', [Validators.required]],
      head_quaters: ['', [Validators.required]],
      website: ['', [Validators.required]],
      established: ['', [Validators.required]],
    });
  }
  //funcion para salvar la información y enviarla a la base de datos
  addAirline() {
    this.submitted = true;
    if (this.airlineForm.valid) {
      this.api.createAirline(this.airlineForm.value).subscribe(
        (response) => {
          this.alert.responseAlert(
            'Correcto!',
            'Aerolinea agregada correctamente'
          );
          this.airlineForm.reset();
        },
        (err) => {
          this.alert.responseAlert(
            'Error',
            'Ocurrió un error agregando la aerolinea'
          );
        }
      );
    }
  }
}
