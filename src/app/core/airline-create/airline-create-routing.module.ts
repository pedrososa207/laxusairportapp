import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AirlineCreatePage } from './airline-create.page';

const routes: Routes = [
  {
    path: '',
    component: AirlineCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AirlineCreatePageRoutingModule {}
