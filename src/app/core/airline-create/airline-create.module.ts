import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AirlineCreatePageRoutingModule } from './airline-create-routing.module';

import { AirlineCreatePage } from './airline-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AirlineCreatePageRoutingModule,
  ],
  declarations: [AirlineCreatePage],
})
export class AirlineCreatePageModule {}
