import { AlertsService } from './../../services/alerts.service';
import { LoadingController, AlertController } from '@ionic/angular';
import { EndpointsService } from './../../services/endpoints.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-passenger-list',
  templateUrl: './passenger-list.page.html',
  styleUrls: ['./passenger-list.page.scss'],
})
export class PassengerListPage implements OnInit {
  passengers: any = [];
  id: string = '';
  page: number = 0;
  totalPages: number = 0;
  constructor(
    private api: EndpointsService,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private alert: AlertsService
  ) {}
  // ejecuta la llamada a los pasajeros para cargarlos en la vista
  ngOnInit() {
    this.getPassengerList();
  }
  // funcion creada para traer pasajeros con limite de 20
  getPassengerList() {
    // this.presentLoading();
    this.api.getAllPassengers(this.page, 20).subscribe(
      (_passengers: any) => {
        this.passengers = _passengers.data;
        this.totalPages = _passengers.totalPages;
      },
      (err) => {
        this.alert.responseAlert(
          'Error',
          'Ocurrió un error mientras se cargaban los pasajeros'
        );
      }
    );
  }
  // Search By ID se utiliza para traer la data de un pasajero en especifico en el filtro
  searchById() {
    console.log(this.id);
    this.passengers = [];
    this.api.getPassengerById(this.id).subscribe((airline) => {
      console.log(airline);
      this.passengers.push(airline);
    });
  }
  // lleva la paginación a la primera pagina
  getFirst() {
    this.page = 0;
    this.getPassengerList();
  }
  // manda la paginación una pagina hacia atras
  getBack() {
    if (this.page > 0) {
      --this.page;
      this.getPassengerList();
    }
  }
  // manda la paginación una pagina hacia adelante
  getNext() {
    ++this.page;
    this.getPassengerList();
  }
  // mueve la paginacion hasta la ultima hoja
  getLast() {
    this.page = this.totalPages - 1;
    this.getPassengerList();
  }
  // funcion para eliminar un pasajero recibiendo un id como parametro para eliminar el pasajero correcto
  deletePassenger(id) {
    this.api.deletePassenger(id).subscribe(
      (response) => {
        this.alert
          .responseAlert('Correcto', 'Pasajero eliminado correctamente')
          .then(() => {
            this.getPassengerList();
          });
      },
      (err) => {
        this.alert.responseAlert(
          'Error',
          'Ocurrió un error al eliminar el pasajero'
        );
      }
    );
  }
  // confirmación para validar que estamos seguros de eliminar ese pasajero
  async confirmDelete(id) {
    const alert = await this.alertController.create({
      header: 'Confirmar',
      message: 'Estas seguro que deseas eliminar este pasajero?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
        {
          text: 'Eliminar',
          handler: () => {
            this.deletePassenger(id);
          },
        },
      ],
    });

    await alert.present();
  }
  // limpia el filtro y carga nuevamente todas las aerolineas
  clean() {
    this.id = '';
    this.getPassengerList();
  }

  // loader para saber cuando sean recibidos los datos
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      spinner: 'bubbles',
    });
    await loading.present();
  }
}
