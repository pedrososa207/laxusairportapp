import { ActivatedRoute } from '@angular/router';
import { AlertsService } from './../../services/alerts.service';
import { EndpointsService } from './../../services/endpoints.service';
import { LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-passenger-create',
  templateUrl: './passenger-create.page.html',
  styleUrls: ['./passenger-create.page.scss'],
})
export class PassengerCreatePage implements OnInit {
  passengerForm: FormGroup;
  airlines: any[];
  id: string = '';
  title: string = 'Agregar pasajero';
  constructor(
    private builder: FormBuilder,
    private api: EndpointsService,
    private loadingController: LoadingController,
    private alert: AlertsService,
    private active: ActivatedRoute
  ) {}
  // Al iniciar, carga el formulario, y trae una lista de las ultimas 10 aerolineas agregadas para poder seleccionar una de las ultimas
  ngOnInit() {
    this.loadForm();
    this.getAirlineList();
    this.active.params.subscribe((data) => {
      // aqui valida si se recibe un ID o no, para pasar a editar o a crear
      console.log(data.id);
      this.id = data.id;
      if (data.id != undefined) {
        this.title = 'Editar pasajero';
        this.searchById();
      }
    });
  }
  // carga del formulario de registro de pasajeros
  loadForm() {
    this.passengerForm = this.builder.group({
      name: ['', [Validators.required]],
      trips: ['', [Validators.required]],
      airline: ['', [Validators.required]],
    });
  }
  // Aqui cargan las ultimas 10 aerolineas con un bucle inverso
  getAirlineList() {
    this.presentLoading();
    this.api.getAllAirlines().subscribe(
      (_airlines: any) => {
        this.airlines = [];
        for (let i = _airlines.length; i > _airlines.length - 20; i--) {
          let air_line = _airlines[i - 1];
          air_line.id = i;
          this.airlines.push(air_line);
        }
        this.loadingController.dismiss();
      },
      (err) => {
        this.loadingController.dismiss();
      }
    );
  }
  // Search By ID se utiliza para traer la data actual del pasajero que se pretende actualizar y asi estar claros de que es lo que queremos cambiar exactamente
  searchById() {
    this.api.getPassengerById(this.id).subscribe((pass: any) => {
      this.passengerForm.controls['name'].patchValue(pass.name);
      this.passengerForm.controls['trips'].patchValue(pass.trips);
      this.passengerForm.controls['airline'].patchValue(pass.airline[0].id);
    });
  }

  // Guarda los datos del nuevo pasajero y lo asigna a una aerolinea
  addPassenger() {
    this.api.createPassenger(this.passengerForm.value).subscribe(
      (response) => {
        this.alert.responseAlert(
          'Correcto!',
          'Pasajero agregado correctamente'
        );
        this.passengerForm.reset();
      },
      (err) => {
        this.alert.responseAlert(
          'Error',
          'Ocurrió un error agregando el pasajero'
        );
      }
    );
  }
  // como el nombre indica, se utiliza para actualizar los datos de un pasajero ya registrado
  updatePassenger() {
    this.api.updatePassenger(this.id, this.passengerForm.value).subscribe(
      (response) => {
        this.alert.responseAlert(
          'Correcto!',
          'Pasajero actualizado correctamente'
        );
        this.passengerForm.reset();
      },
      (err) => {
        this.alert.responseAlert(
          'Error',
          'Ocurrió un error actualizando el pasajero'
        );
      }
    );
  }
  // aqui valida si se recibe un ID o no, para pasar a editar o a crear
  valid() {
    if (this.id != undefined) {
      this.updatePassenger();
    } else {
      this.addPassenger();
    }
  }
  // loader para saber cuando carga exactamente la data
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando aerolineas...',
      spinner: 'bubbles',
    });
    await loading.present();
  }
}
