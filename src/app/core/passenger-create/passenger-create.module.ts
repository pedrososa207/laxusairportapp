import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PassengerCreatePageRoutingModule } from './passenger-create-routing.module';

import { PassengerCreatePage } from './passenger-create.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    PassengerCreatePageRoutingModule,
  ],
  declarations: [PassengerCreatePage],
})
export class PassengerCreatePageModule {}
