import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PassengerCreatePage } from './passenger-create.page';

const routes: Routes = [
  {
    path: '',
    component: PassengerCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PassengerCreatePageRoutingModule {}
