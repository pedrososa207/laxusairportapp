import { EndpointsService } from './../../services/endpoints.service';
import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-airline-list',
  templateUrl: './airline-list.page.html',
  styleUrls: ['./airline-list.page.scss'],
})
export class AirlineListPage implements OnInit {
  airlines: any = [];
  id: string = '';

  constructor(
    private api: EndpointsService,
    private loadingController: LoadingController
  ) {}

  ngOnInit(): void {
    this.getAirlineList();
  }
  // llama todas las aerolineas, como son demasiadas, ejecute un bucle inverso para que me trajera las ultimas 10, porque ese endpoint no tiene paginación
  getAirlineList() {
    this.presentLoading();
    this.api.getAllAirlines().subscribe(
      (_airlines: any) => {
        this.airlines = [];
        for (let i = _airlines.length; i > _airlines.length - 20; i--) {
          this.airlines.push(_airlines[i - 1]);
        }
        this.loadingController.dismiss();
      },
      (err) => {
        this.loadingController.dismiss();
      }
    );
  }
  // filtra por ID aerolineas
  searchById() {
    this.airlines = [];
    this.api.getAirlineById(this.id).subscribe((airline) => {
      this.airlines.push(airline);
    });
  }
  // limpia el filtro y llama las aerolinas nuevamente
  clean() {
    this.id = '';
    this.getAirlineList();
  }
  // loader, para saber cuando termina de cargar la data
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      spinner: 'bubbles',
    });
    await loading.present();
  }
}
