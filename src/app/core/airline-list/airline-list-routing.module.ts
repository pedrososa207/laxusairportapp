import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AirlineListPage } from './airline-list.page';

const routes: Routes = [
  {
    path: '',
    component: AirlineListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AirlineListPageRoutingModule {}
