import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AirlineListPageRoutingModule } from './airline-list-routing.module';

import { AirlineListPage } from './airline-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AirlineListPageRoutingModule
  ],
  declarations: [AirlineListPage]
})
export class AirlineListPageModule {}
