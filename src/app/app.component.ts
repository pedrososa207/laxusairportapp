import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  image = 'https://upload.wikimedia.org/wikipedia/commons/e/e3/Logo_vector.png';
  public appPages = [
    { title: 'Aerolineas', url: '/airline-list', icon: 'mail' },
    { title: 'Crear aerolinea', url: '/airline-create', icon: 'paper-plane' },
    { title: 'Pasajeros', url: '/passenger-list', icon: 'heart' },
    { title: 'Crear pasajero', url: '/passenger-create', icon: 'archive' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() {}
}
