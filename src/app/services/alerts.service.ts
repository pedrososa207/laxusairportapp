import { AlertController } from '@ionic/angular';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AlertsService {
  constructor(private alertController: AlertController) {}

  async responseAlert(title, sms) {
    const alert = await this.alertController.create({
      header: title,
      message: sms,
      buttons: ['OK'],
    });

    await alert.present();
  }
}
