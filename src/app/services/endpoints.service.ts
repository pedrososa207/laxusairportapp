import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class EndpointsService {
  constructor(private http: HttpClient) {}
  // airlines endpoints
  getAllAirlines() {
    return this.http.get('https://api.instantwebtools.net/v1/airlines');
  }
  createAirline(data: any) {
    return this.http.post('https://api.instantwebtools.net/v1/airlines', data);
  }

  getAirlineById(id: string) {
    return this.http.get('https://api.instantwebtools.net/v1/airlines/' + id);
  }
  // passengers endpoints
  getAllPassengers(page, size) {
    return this.http.get(
      `https://api.instantwebtools.net/v1/passenger?page=${page}&size=${size}`
    );
  }

  createPassenger(data: any) {
    return this.http.post('https://api.instantwebtools.net/v1/passenger', data);
  }
  getPassengerById(id: string) {
    return this.http.get('https://api.instantwebtools.net/v1/passenger/' + id);
  }
  deletePassenger(id: string) {
    return this.http.delete(
      'https://api.instantwebtools.net/v1/passenger/' + id
    );
  }
  updatePassenger(id: string, data: any) {
    return this.http.put(
      'https://api.instantwebtools.net/v1/passenger/' + id,
      data
    );
  }
}
