import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'airline-list',
    pathMatch: 'full',
  },
  {
    path: 'airline-list',
    loadChildren: () =>
      import('./core/airline-list/airline-list.module').then(
        (m) => m.AirlineListPageModule
      ),
  },
  {
    path: 'airline-create',
    loadChildren: () =>
      import('./core/airline-create/airline-create.module').then(
        (m) => m.AirlineCreatePageModule
      ),
  },
  {
    path: 'passenger-list',
    loadChildren: () =>
      import('./core/passenger-list/passenger-list.module').then(
        (m) => m.PassengerListPageModule
      ),
  },
  {
    path: 'passenger-create',
    loadChildren: () =>
      import('./core/passenger-create/passenger-create.module').then(
        (m) => m.PassengerCreatePageModule
      ),
  },
  {
    path: 'passenger-create/:id',
    loadChildren: () =>
      import('./core/passenger-create/passenger-create.module').then(
        (m) => m.PassengerCreatePageModule
      ),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
